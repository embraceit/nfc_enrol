package com.embrace.nfcprovision.utils;

import android.content.Context;
import android.os.Handler;
import android.util.AttributeSet;

import com.daimajia.swipe.SimpleSwipeListener;
import com.daimajia.swipe.SwipeLayout;

public class CSwipeLayout extends SwipeLayout {
    public enum SwipeStatus {OPENING, OPENED, CLOSING, CLOSED}

    SwipeStatus swipeStatus = SwipeStatus.CLOSED;

    public CSwipeLayout(Context context) {
        super(context);

        init();
    }

    public CSwipeLayout(Context context, AttributeSet attrs) {
        super(context, attrs);

        init();
    }

    public CSwipeLayout(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);

        init();
    }

    private void init() {
        addSwipeListener(new SimpleSwipeListener() {
            Handler handler = new Handler();
            Runnable closeRunnable = new Runnable() {
                @Override
                public void run() {
                    swipeStatus = SwipeStatus.CLOSED;
                }
            };
            Runnable openRunnable = new Runnable() {
                @Override
                public void run() {
                    swipeStatus = SwipeStatus.OPENED;
                }
            };

            @Override
            public void onStartOpen(SwipeLayout layout) {
                swipeStatus = SwipeStatus.OPENING;
                handler.removeCallbacks(closeRunnable);
            }

            @Override
            public void onOpen(SwipeLayout layout) {
                handler.removeCallbacks(closeRunnable);
                handler.postDelayed(openRunnable, 100);
            }

            @Override
            public void onStartClose(SwipeLayout layout) {
                swipeStatus = SwipeStatus.CLOSING;
                handler.removeCallbacks(closeRunnable);
            }

            @Override
            public void onClose(SwipeLayout layout) {
                handler.removeCallbacks(openRunnable);
                handler.postDelayed(closeRunnable, 100);
            }
        });
    }

    public SwipeStatus getSwipeStatus() {
        return swipeStatus;
    }
}
