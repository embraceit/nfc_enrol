package com.embrace.nfcprovision.utils

import android.text.Editable
import android.text.TextWatcher

/**
 * This class wraps {@link TextWatcher} and delegates {@link TextWatcher#afterTextChanged} event to
 * {@link OnTextChangedListener} while hiding all other unnecessary events.
 */
class TextWatcherWrapper(
    private val id: Int,
    private val listener: OnTextChangedListener
) : TextWatcher {
    override fun afterTextChanged(p0: Editable?) {
        listener.onTextChanged(id, p0?.toString())
    }

    override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
        // Ignore
    }

    override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
        // Ignore
    }

    interface OnTextChangedListener {
        fun onTextChanged(id: Int, s: String?)
    }
}