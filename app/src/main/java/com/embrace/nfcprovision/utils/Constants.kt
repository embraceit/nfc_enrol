package com.embrace.nfcprovision.utils

import android.content.Intent

object Constants {
    val FLAG_NEW = listOf(Intent.FLAG_ACTIVITY_CLEAR_TOP, Intent.FLAG_ACTIVITY_NEW_TASK)
}