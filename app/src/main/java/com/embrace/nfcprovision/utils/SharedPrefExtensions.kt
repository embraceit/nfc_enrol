package com.embrace.nfcprovision.utils

import android.content.SharedPreferences
import com.google.gson.GsonBuilder

fun <T : Any?> SharedPreferences.Editor.putObject(key: String, obj: T?): SharedPreferences.Editor {
    val gson = GsonBuilder().serializeNulls().create()
    val json = gson.toJson(obj)
    return putString(key, json)
}

fun <T : Any?> SharedPreferences.getObject(
    key: String,
    kClass: Class<T>,
    defaultValue: T? = null
): T? {
    val gson = GsonBuilder().serializeNulls().create()
    val obj = getString(key, "")
    if (obj == null || obj.isEmpty())
        return defaultValue
    return gson.fromJson(obj, kClass)
}

