package com.embrace.nfcprovision.base

import android.content.Context
import android.content.SharedPreferences

abstract class BaseSharedPref(protected val context: Context) {
    protected abstract val PREF_NAME: String

    protected val sharedPreference: SharedPreferences
        get() {
            return context.applicationContext.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE)
        }

    fun clear() {
        sharedPreference.edit().clear().apply()
    }
}