package com.embrace.nfcprovision.base

import androidx.annotation.IdRes
import androidx.appcompat.app.AppCompatActivity

abstract class BaseActivity : AppCompatActivity() {
    abstract val TAG: String

    fun replaceWithFragment(@IdRes id: Int, fragment: BaseFragment) {
        if (supportFragmentManager.findFragmentByTag(fragment.TAG) == null) {
            supportFragmentManager.beginTransaction()
                .replace(id, fragment, fragment.TAG)
                .commit()
        }
    }
}