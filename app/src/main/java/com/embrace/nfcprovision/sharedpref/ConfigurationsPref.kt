package com.embrace.nfcprovision.sharedpref

import android.content.Context
import com.embrace.nfcprovision.Provision
import com.embrace.nfcprovision.base.BaseSharedPref
import com.embrace.nfcprovision.utils.getObject
import com.embrace.nfcprovision.utils.putObject

class ConfigurationsPref(context: Context) : BaseSharedPref(context) {
    override val PREF_NAME: String = "config"

    fun getAllConfigurations(): ArrayList<Provision> {
        return sharedPreference.all.keys.mapNotNull {
            sharedPreference.getObject(it, Provision::class.java)
        } as ArrayList<Provision>
    }

    fun getConfiguration(key: String): Provision? {
        return sharedPreference.getObject(key, Provision::class.java)
    }

    fun saveConfiguration(key: String, configuration: Provision) {
        sharedPreference.edit().putObject(key, configuration).apply()
    }

    fun deleteConfiguration(key: String) {
        if (sharedPreference.contains(key))
            sharedPreference.edit().remove(key).apply()
    }
}