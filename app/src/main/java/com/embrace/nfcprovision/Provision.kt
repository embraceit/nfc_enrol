package com.embrace.nfcprovision

import java.io.Serializable

data class Provision(
    var timestamp: Long? = null,

    var wifiSSID: String? = null,
    var wifiSecurityType: String? = null,
    var wifiPassword: String? = null,

    var deviceAdminPackageName: String? = null,
    var deviceAdminComponentName: String? = null,
    var deviceAdminSignChecksum: String? = null,
    var deviceAdminPackageDownloadLocation: String? = null,

    var enrolmentToken: String? = null
) : Serializable