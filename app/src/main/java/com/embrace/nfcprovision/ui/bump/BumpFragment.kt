package com.embrace.nfcprovision.ui.bump

import android.app.admin.DevicePolicyManager
import android.content.Context
import android.nfc.NdefMessage
import android.nfc.NdefRecord
import android.nfc.NfcAdapter
import android.nfc.NfcEvent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import com.bumptech.glide.Glide
import com.embrace.nfcprovision.Provision
import com.embrace.nfcprovision.R
import com.embrace.nfcprovision.base.BaseActivity
import com.embrace.nfcprovision.base.BaseFragment
import com.embrace.nfcprovision.ui.configuration.ConfigurationListActivity
import kotlinx.android.synthetic.main.fragment_bump.*
import java.io.ByteArrayOutputStream
import java.io.IOException
import java.io.StringWriter
import java.util.*

/**
 * A simple [BaseFragment] subclass.
 * Use the [BumpFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class BumpFragment : BaseFragment(),
    NfcAdapter.CreateNdefMessageCallback {
    companion object {
        @JvmStatic
        fun newInstance(provision: Provision?) = BumpFragment().apply {
            arguments = bundleOf("provision" to provision)
        }
    }

    override val TAG: String = "BumpFragment"

    var provision = Provision()

    override fun onAttach(context: Context) {
        super.onAttach(context)
        setupActionBar()
    }

    private fun setupActionBar() {
        (requireActivity() as BaseActivity).supportActionBar?.apply {
            title = ""
            setDisplayShowHomeEnabled(true)
            setDisplayHomeAsUpEnabled(true)
            setHomeAsUpIndicator(R.drawable.ic_outline_arrow_back_ios_24)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.getSerializable("provision")?.let {
            provision = it as Provision
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        setHasOptionsMenu(true)
        return inflater.inflate(R.layout.fragment_bump, container, false)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                activity?.onBackPressed()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        // setting gif
        Glide.with(iv_hint)
            .asGif()
            .load(R.drawable.nfc_bump)
            .into(iv_hint)

        // setting up NFC adapter
        val adapter = NfcAdapter.getDefaultAdapter(context)
        adapter.setNdefPushMessageCallback(this, activity)

        // setting listeners
        ll_done.setOnClickListener {
            ConfigurationListActivity.start(requireContext())
        }
    }

    override fun createNdefMessage(p0: NfcEvent?): NdefMessage? {
        val stream = ByteArrayOutputStream()
        val properties = Properties()

        properties[DevicePolicyManager.EXTRA_PROVISIONING_LOCAL_TIME] =
            System.currentTimeMillis().toString()

        if (!provision.wifiSSID.isNullOrBlank())
            properties[DevicePolicyManager.EXTRA_PROVISIONING_WIFI_SSID] = {
                var value = provision.wifiSSID
                if (value?.startsWith("\"")?.not() == true || value?.endsWith("\"")
                        ?.not() == true
                ) {
                    value = "\"" + value + "\""
                }
                value
            }.invoke()

        if (!provision.wifiSecurityType.isNullOrBlank())
            properties[DevicePolicyManager.EXTRA_PROVISIONING_WIFI_SECURITY_TYPE] =
                provision.wifiSecurityType
        if (!provision.wifiPassword.isNullOrBlank())
            properties[DevicePolicyManager.EXTRA_PROVISIONING_WIFI_PASSWORD] =
                provision.wifiPassword

        if (!provision.deviceAdminComponentName.isNullOrBlank())
            properties[DevicePolicyManager.EXTRA_PROVISIONING_DEVICE_ADMIN_COMPONENT_NAME] =
                provision.deviceAdminComponentName

        if (!provision.deviceAdminSignChecksum.isNullOrBlank())
            properties[DevicePolicyManager.EXTRA_PROVISIONING_DEVICE_ADMIN_SIGNATURE_CHECKSUM] =
                provision.deviceAdminSignChecksum

        if (!provision.deviceAdminPackageDownloadLocation.isNullOrBlank())
            properties[DevicePolicyManager.EXTRA_PROVISIONING_DEVICE_ADMIN_PACKAGE_DOWNLOAD_LOCATION] =
                provision.deviceAdminPackageDownloadLocation


        if (!provision.enrolmentToken.isNullOrBlank()) {
            val adminExtraProperties = Properties()
            adminExtraProperties.setProperty(
                "com.google.android.apps.work.clouddpc.EXTRA_ENROLLMENT_TOKEN",
                provision.enrolmentToken
            )
            val stringWriter = StringWriter()
            try {
                adminExtraProperties.store(stringWriter, "EXTRA_ENROLLMENT_TOKEN")
            } catch (e: IOException) {
                e.printStackTrace()
            }
            properties[DevicePolicyManager.EXTRA_PROVISIONING_ADMIN_EXTRAS_BUNDLE] =
                stringWriter.toString()
        }

        try {
            properties.store(stream, "Test")
            val record = NdefRecord.createMime(
                DevicePolicyManager.MIME_TYPE_PROVISIONING_NFC, stream.toByteArray()
            )
            return NdefMessage(arrayOf(record))
        } catch (ioe: IOException) {
            Log.e(TAG, ioe.localizedMessage)
        }

        return null
    }
}