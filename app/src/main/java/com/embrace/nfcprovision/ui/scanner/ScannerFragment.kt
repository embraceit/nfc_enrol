package com.embrace.nfcprovision.ui.scanner

import android.app.admin.DevicePolicyManager
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.*
import android.widget.Toast
import androidx.core.os.bundleOf
import com.embrace.nfcprovision.Provision
import com.embrace.nfcprovision.R
import com.embrace.nfcprovision.base.BaseActivity
import com.embrace.nfcprovision.base.BaseFragment
import com.embrace.nfcprovision.ui.provision.ConfigurationDetailsActivity
import com.google.zxing.Result
import com.google.zxing.ResultPoint
import com.journeyapps.barcodescanner.BarcodeCallback
import com.journeyapps.barcodescanner.BarcodeResult
import com.journeyapps.barcodescanner.camera.CameraSettings
import kotlinx.android.synthetic.main.fragment_scanner.*
import org.json.JSONException
import org.json.JSONObject


/**
 * A simple [BaseFragment] subclass.
 * Use the [ScannerFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class ScannerFragment : BaseFragment() {
    companion object {
        fun newInstance() = ScannerFragment()
    }

    override val TAG: String = "ScannerFragment"

    override fun onAttach(context: Context) {
        super.onAttach(context)
        setupActionBar()
    }

    private fun setupActionBar() {
        (requireActivity() as BaseActivity).supportActionBar?.apply {
            title = ""
            setDisplayShowHomeEnabled(true)
            setDisplayHomeAsUpEnabled(true)
            setHomeAsUpIndicator(R.drawable.ic_outline_arrow_back_ios_24)
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                activity?.onBackPressed()
                true
            }
            R.id.action_provision -> {
                openProvision()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    private fun openProvision() {
        activity?.finish()
        startActivity(Intent(context, ConfigurationDetailsActivity::class.java))
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        setHasOptionsMenu(true)
        return inflater.inflate(R.layout.fragment_scanner, container, false)
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.menu_scanner, menu)
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupScanner()
        ll_enter_manually.setOnClickListener {
            val intent = Intent(activity, ConfigurationDetailsActivity::class.java)
            startActivity(intent)
        }
        switch_torch.setOnCheckedChangeListener { _, isChecked ->
            if (isChecked)
                dbv_scanner.setTorchOff()
            else
                dbv_scanner.setTorchOn()
        }
    }

    override fun onPause() {
        super.onPause()
        dbv_scanner.pause()
    }

    override fun onResume() {
        super.onResume()
        dbv_scanner.resume()
    }

    private fun setupScanner() {
        dbv_scanner.apply {
            statusView.visibility = View.GONE
            viewFinder.visibility = View.GONE
        }

        val cameraSettings = CameraSettings().apply {
            isAutoFocusEnabled = true
            requestedCameraId = 0
            isAutoTorchEnabled = false
        }
        dbv_scanner.barcodeView.cameraSettings = cameraSettings
        dbv_scanner.resume()
        dbv_scanner.decodeContinuous(object : BarcodeCallback {
            override fun barcodeResult(result: BarcodeResult?) {
                Log.i(TAG, result?.toString() ?: "Result not available")
                handleResult(result?.result)
            }

            override fun possibleResultPoints(resultPoints: MutableList<ResultPoint>?) = Unit
        })
    }

    fun handleResult(result: Result?) {
        val intent = Intent(context, ConfigurationDetailsActivity::class.java)
        val qrText: String = result?.text ?: ""
        try {
            val jsonObject = JSONObject(qrText)
            if (jsonObject.has(DevicePolicyManager.EXTRA_PROVISIONING_ADMIN_EXTRAS_BUNDLE)) {
                val extraJson = JSONObject(
                    jsonObject.getNullableString(DevicePolicyManager.EXTRA_PROVISIONING_ADMIN_EXTRAS_BUNDLE)
                        ?: ""
                )

                val provision = Provision(
                    wifiSSID = jsonObject.getNullableString(DevicePolicyManager.EXTRA_PROVISIONING_WIFI_SSID),
                    wifiPassword = jsonObject.getNullableString(DevicePolicyManager.EXTRA_PROVISIONING_WIFI_PASSWORD),
                    wifiSecurityType = jsonObject.getNullableString(DevicePolicyManager.EXTRA_PROVISIONING_WIFI_SECURITY_TYPE),

                    deviceAdminComponentName = jsonObject.getNullableString(DevicePolicyManager.EXTRA_PROVISIONING_DEVICE_ADMIN_COMPONENT_NAME),
                    deviceAdminSignChecksum = jsonObject.getNullableString(DevicePolicyManager.EXTRA_PROVISIONING_DEVICE_ADMIN_SIGNATURE_CHECKSUM),
                    deviceAdminPackageDownloadLocation = jsonObject.getNullableString(
                        DevicePolicyManager.EXTRA_PROVISIONING_DEVICE_ADMIN_PACKAGE_DOWNLOAD_LOCATION
                    ),

                    enrolmentToken = extraJson.getNullableString("com.google.android.apps.work.clouddpc.EXTRA_ENROLLMENT_TOKEN")
                )

                intent.putExtra("data", bundleOf("provision" to provision))
                startActivity(intent)
                activity?.finish()
            } else {
                Toast.makeText(
                    activity,
                    "Unable to scan QR code, please try again",
                    Toast.LENGTH_SHORT
                ).show()
            }

        } catch (e: JSONException) {
            e.printStackTrace()
        }
    }

    private fun JSONObject.getNullableString(key: String): String? {
        return try {
            getString(key)
        } catch (ex: JSONException) {
            null
        }
    }
}