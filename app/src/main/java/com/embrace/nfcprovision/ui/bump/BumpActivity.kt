package com.embrace.nfcprovision.ui.bump

import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.core.os.bundleOf
import com.embrace.nfcprovision.Provision
import com.embrace.nfcprovision.R
import com.embrace.nfcprovision.base.BaseActivity

class BumpActivity : BaseActivity() {
    override val TAG: String = "BumpActivity"

    companion object {
        @JvmStatic
        fun start(context: Context, provision: Provision) {
            val intent = Intent(context, BumpActivity::class.java)
                .putExtra("data", bundleOf("provision" to provision))
            context.startActivity(intent)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_configuration_details)

        val provision = intent.getBundleExtra("data")?.getSerializable("provision") as Provision?
        replaceWithFragment(R.id.container, BumpFragment.newInstance(provision))
    }
}