package com.embrace.nfcprovision.ui.provision

import android.Manifest
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.nfc.NfcManager
import android.os.Bundle
import android.view.*
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.core.os.bundleOf
import com.embrace.nfcprovision.Provision
import com.embrace.nfcprovision.R
import com.embrace.nfcprovision.base.BaseActivity
import com.embrace.nfcprovision.base.BaseFragment
import com.embrace.nfcprovision.sharedpref.ConfigurationsPref
import com.embrace.nfcprovision.ui.bump.BumpActivity
import com.embrace.nfcprovision.ui.scanner.ScannerActivity
import com.embrace.nfcprovision.utils.TextWatcherWrapper
import kotlinx.android.synthetic.main.fragment_configuration_details.*


/**
 * A simple [BaseFragment] subclass.
 * Use the [ConfigurationDetailsFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class ConfigurationDetailsFragment : BaseFragment(), TextWatcherWrapper.OnTextChangedListener {
    companion object {
        fun newInstance(provision: Provision?) = ConfigurationDetailsFragment().apply {
            arguments = bundleOf("provision" to provision)
        }
    }

    override val TAG: String = "ProvisionFragment"

    private lateinit var configSharedPref: ConfigurationsPref

    override fun onAttach(context: Context) {
        super.onAttach(context)
        configSharedPref = ConfigurationsPref(context.applicationContext)
        setupActionBar()
    }

    private var provision = Provision()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        arguments?.getSerializable("provision")?.let {
            provision = it as Provision
        }
    }

    private fun setupActionBar() {
        (requireActivity() as BaseActivity).supportActionBar?.apply {
            title = ""
            setDisplayShowHomeEnabled(true)
            setDisplayHomeAsUpEnabled(true)
            setHomeAsUpIndicator(R.drawable.ic_outline_arrow_back_ios_24)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        setHasOptionsMenu(true)
        return inflater.inflate(R.layout.fragment_configuration_details, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initialize()
        setListeners()
    }

    private fun setListeners() {
        ll_start_enrolment.setOnClickListener {
            provision.enrolmentToken?.let {
                provision.timestamp = System.currentTimeMillis()
                configSharedPref.saveConfiguration(it, provision)
                checkNFC()
            } ?: run {
                Toast.makeText(
                    requireContext(),
                    "Enrolment token is required to start enrolment process",
                    Toast.LENGTH_LONG
                ).show()
            }
        }

        ib_back.setOnClickListener {
            activity?.finish()
        }

        et_ssid.addTextChangedListener(TextWatcherWrapper(R.id.et_ssid, this))
        et_secuirty.addTextChangedListener(TextWatcherWrapper(R.id.et_secuirty, this))
        et_password.addTextChangedListener(TextWatcherWrapper(R.id.et_password, this))
        et_token.addTextChangedListener(TextWatcherWrapper(R.id.et_token, this))
    }

    private fun checkNFC() {
        val manager = requireContext().getSystemService(Context.NFC_SERVICE) as NfcManager
        val adapter = manager.defaultAdapter

        // check to see if NFC is supported/enabled
        // if so, then take user to bump activity
        if (adapter != null && adapter.isEnabled) {
            BumpActivity.start(requireContext(), provision)
            activity?.finish()

        } else if (adapter != null && !adapter.isEnabled) {
            Toast.makeText(context, "Please enable NFC to continue", Toast.LENGTH_LONG).show()
            startActivity(Intent(android.provider.Settings.ACTION_WIRELESS_SETTINGS))
        } else {
            Toast.makeText(context, "NFC not supported", Toast.LENGTH_LONG).show()
        }
    }

    private fun initialize() {
        et_ssid.setText(provision.wifiSSID)
        et_secuirty.setText(provision.wifiSecurityType)
        et_password.setText(provision.wifiPassword)
        et_token.setText(provision.enrolmentToken)
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.menu_provision, menu)
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                activity?.onBackPressed()
                true
            }
            R.id.action_scanner -> {
                checkCameraPermission()
                true
            }
            R.id.action_clear -> {
                provision = Provision()

                clearFields()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    private fun clearFields() {
        et_ssid.text?.clear()
        et_secuirty.text?.clear()
        et_password.text?.clear()
        et_token.text?.clear()
    }

    private fun checkCameraPermission() {
        if (ContextCompat.checkSelfPermission(requireContext(), Manifest.permission.CAMERA)
            == PackageManager.PERMISSION_GRANTED
        ) {
            launchScanner()
        } else {
            if (shouldShowRequestPermissionRationale(Manifest.permission.CAMERA)) {
                requestCameraPermission()
            } else {
                showCameraPermissionDenied()
            }
        }
    }

    private fun requestCameraPermission() {
        requestPermissions(arrayOf(Manifest.permission.CAMERA), 101)
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        when (requestCode) {
            101 -> {
                when {
                    grantResults.isEmpty() -> {
                        Toast.makeText(context, "Camera permission denied", Toast.LENGTH_LONG)
                            .show()
                    }
                    grantResults[0] == PackageManager.PERMISSION_GRANTED -> {
                        launchScanner()
                    }
                    else -> {
                        showCameraPermissionDenied()
                    }
                }
            }
            else -> super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        }
    }

    private fun showCameraPermissionDenied() {
        // Todo: Replace with snackbar
        Toast.makeText(
            context,
            "Camera permission denied, Please grant camera permission from ic_settings",
            Toast.LENGTH_LONG
        ).show()
    }

    private fun launchScanner() {
        activity?.finish()
        ScannerActivity.start(requireContext())
    }

    override fun onTextChanged(id: Int, text: String?) {
        var text = text
        if (text.isNullOrBlank())
            text = null
        when (id) {
            R.id.et_ssid -> {
                provision.wifiSSID = text
            }
            R.id.et_secuirty -> {
                provision.wifiSecurityType = text
            }
            R.id.et_password -> {
                provision.wifiPassword = text
            }
            R.id.et_token -> {
                provision.enrolmentToken = text
            }
        }
    }
}