package com.embrace.nfcprovision.ui.configuration.adapter

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.embrace.nfcprovision.Provision
import kotlinx.android.synthetic.main.list_item_configuration.view.*
import java.text.SimpleDateFormat
import java.util.*

class ConfigViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
    fun bind(
        config: Provision,
        actionListener: ConfigActionListener
    ) {
        // Binding
        itemView.tv_enrolmentToken.text = config.enrolmentToken ?: "N/A"

        val sdf = SimpleDateFormat("dd")
        itemView.tv_date.text = sdf.format(Date(config.timestamp ?: System.currentTimeMillis()))
        sdf.applyPattern("EEE")
        itemView.tv_day.text = sdf.format(Date(config.timestamp ?: System.currentTimeMillis()))

        if (config.wifiSSID != null) {
            itemView.tv_wifi_ssid.text = config.wifiSSID
            itemView.tv_wifi_ssid.visibility = View.VISIBLE
        } else {
            itemView.tv_wifi_ssid.visibility = View.GONE
        }

        if(config.wifiSecurityType != null) {
            itemView.tv_wifi_secuirty.text = config.wifiSecurityType
            itemView.tv_wifi_secuirty.visibility = View.VISIBLE
        } else {
            itemView.tv_wifi_secuirty.visibility = View.GONE
        }

        // Listeners
        itemView.tv_edit.setOnClickListener {
            actionListener.onConfigurationEdit(config)
        }

        itemView.tv_delete.setOnClickListener {
            actionListener.onConfigurationDelete(config)
        }

        itemView.ll_configuration.setOnClickListener {
            actionListener.onConfigurationClick(config)
        }
    }
}