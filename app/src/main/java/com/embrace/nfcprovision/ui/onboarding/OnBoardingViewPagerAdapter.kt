package com.embrace.nfcprovision.ui.onboarding

import android.content.Context
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import com.embrace.nfcprovision.R

class OnBoardingViewPagerAdapter(manager: FragmentManager,
                                 private val context : Context) :
    FragmentPagerAdapter(manager, BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT) {

    // Returns total number of pages
    override fun getCount(): Int {
        return NUM_ITEMS
    }

    // Returns the fragment to display for that page
    override fun getItem(position: Int): Fragment {
        return when (position) {
            0 -> OnBoardingFragment.newInstance(
                context.resources.getString(R.string.title_on_boarding_1),
                context.resources.getString(R.string.description_on_boarding_1),
                R.raw.lottie_scan_qr_code
            )
            1 -> OnBoardingFragment.newInstance(
                context.resources.getString(R.string.title_on_boarding_2),
                context.resources.getString(R.string.description_on_boarding_2),
                R.raw.lottie_wifi_1
            )
            2 -> OnBoardingFragment.newInstance(
                context.resources.getString(R.string.title_on_boarding_3),
                context.resources.getString(R.string.description_on_boarding_3),
                R.raw.lottie_nfc_1
            )
            else -> null
        }!!
    }

    // Returns the page title for the top indicator
    override fun getPageTitle(position: Int): CharSequence? {
        return "Page $position"
    }

    

    companion object {
        private const val NUM_ITEMS = 3
    }

}