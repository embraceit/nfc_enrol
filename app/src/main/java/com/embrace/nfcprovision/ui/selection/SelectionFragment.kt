package com.embrace.nfcprovision.ui.selection

import android.Manifest
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.content.ContextCompat
import com.embrace.nfcprovision.R
import com.embrace.nfcprovision.base.BaseFragment
import com.embrace.nfcprovision.ui.provision.ConfigurationDetailsActivity
import com.embrace.nfcprovision.ui.scanner.ScannerActivity
import kotlinx.android.synthetic.main.fragment_selection.*

/**
 * A simple [BaseFragment] subclass.
 * Use the [SelectionFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class SelectionFragment : BaseFragment() {
    companion object {
        fun newInstance() = SelectionFragment()
    }

    override val TAG: String = "SelectionFragment"

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_selection, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setListeners()
    }

    private fun setListeners() {
        mbtn_scan.setOnClickListener {
            checkCameraPermission()
        }

        mbtn_provision.setOnClickListener {
            startActivity(Intent(context, ConfigurationDetailsActivity::class.java))
        }
    }

    private fun checkCameraPermission() {
        if (ContextCompat.checkSelfPermission(requireContext(), Manifest.permission.CAMERA)
            == PackageManager.PERMISSION_GRANTED
        ) {
            launchScanner()
        } else {
            requestCameraPermission()
        }
    }

    private fun requestCameraPermission() {
        requestPermissions(arrayOf(Manifest.permission.CAMERA), 101)
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        when (requestCode) {
            101 -> {
                when {
                    grantResults.isEmpty() -> {
                        Toast.makeText(context, "Camera permission denied", Toast.LENGTH_LONG)
                            .show()
                    }
                    grantResults[0] == PackageManager.PERMISSION_GRANTED -> {
                        launchScanner()
                    }
                    else -> {
                        showCameraPermissionDenied()
                    }
                }
            }
            else -> super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        }
    }

    private fun showCameraPermissionDenied() {
        // Todo: Replace with snackbar
        Toast.makeText(
            context,
            "Camera permission denied, Please grant camera permission from ic_settings",
            Toast.LENGTH_LONG
        ).show()
    }

    private fun launchScanner() {
        ScannerActivity.start(requireContext())
    }
}