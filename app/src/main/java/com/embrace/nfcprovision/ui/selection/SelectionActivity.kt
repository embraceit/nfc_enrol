package com.embrace.nfcprovision.ui.selection

import android.content.Context
import android.content.Intent
import android.os.Bundle
import com.embrace.nfcprovision.R
import com.embrace.nfcprovision.base.BaseActivity

class SelectionActivity : BaseActivity() {
    override val TAG: String = "SelectionActivity"

    companion object {
        fun start(context: Context) {
            val intent = Intent(context, SelectionActivity::class.java)
            context.startActivity(intent)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_configuration_details)
        replaceWithFragment(R.id.container, SelectionFragment.newInstance())
    }
}