package com.embrace.nfcprovision.ui.configuration


import android.content.Context
import android.content.Intent
import android.nfc.NfcManager
import android.os.Bundle
import android.view.View
import android.widget.LinearLayout
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.embrace.nfcprovision.Provision
import com.embrace.nfcprovision.R
import com.embrace.nfcprovision.sharedpref.ConfigurationsPref
import com.embrace.nfcprovision.ui.bump.BumpActivity
import com.embrace.nfcprovision.ui.configuration.adapter.ConfigActionListener
import com.embrace.nfcprovision.ui.configuration.adapter.ConfigAdapter
import com.embrace.nfcprovision.ui.provision.ConfigurationDetailsActivity
import com.embrace.nfcprovision.ui.scanner.ScannerActivity
import kotlinx.android.synthetic.main.activity_configuration_list.*

class ConfigurationListActivity : AppCompatActivity(), ConfigActionListener {
    companion object {
        fun start(context: Context, flags: List<Int> = listOf()) {
            val intent = Intent(context, ConfigurationListActivity::class.java)
            flags.forEach { flag -> intent.addFlags(flag) }
            context.startActivity(intent)
        }
    }

    private lateinit var btnStart: LinearLayout
    private val configSharedPref by lazy { ConfigurationsPref(applicationContext) }
    private val configs = arrayListOf<Provision>()
    private val configAdapter by lazy { ConfigAdapter(configs, this) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_configuration_list)

        initialize()
        setListeners()
    }

    private fun initialize() {
        btnStart = layout_start
        rv_configurations.layoutManager = LinearLayoutManager(this)
        rv_configurations.adapter = configAdapter
    }

    private fun setListeners() {
        btnStart.setOnClickListener {
            ScannerActivity.start(this)
        }
    }

    override fun onResume() {
        super.onResume()
        configs.clear()
        configs.addAll(configSharedPref.getAllConfigurations())
        configAdapter.closeAllItems()
        configAdapter.notifyDataSetChanged()
        toggleNoItemVisibility(configs.isEmpty())
    }

    private fun toggleNoItemVisibility(visible: Boolean) {
        if (visible) {
            rl_noItem.visibility = View.VISIBLE
            ll_configuration.visibility = View.GONE
            iv_task_list.visibility = View.GONE
        } else {
            rl_noItem.visibility = View.GONE
            iv_task_list.visibility = View.VISIBLE
            ll_configuration.visibility = View.VISIBLE
        }
    }

    private fun checkNFC(config: Provision) {
        val manager = getSystemService(Context.NFC_SERVICE) as NfcManager
        val adapter = manager.defaultAdapter

        // check to see if NFC is supported/enabled
        // if so, then take user to bump activity
        if (adapter != null && adapter.isEnabled) {
            BumpActivity.start(this, config)

        } else if (adapter != null && !adapter.isEnabled) {
            Toast.makeText(this, "Please enable NFC to continue", Toast.LENGTH_LONG).show()
            startActivity(Intent(android.provider.Settings.ACTION_WIRELESS_SETTINGS))
        } else {
            Toast.makeText(this, "NFC not supported", Toast.LENGTH_LONG).show()
        }
    }

    /*
    * ConfigActionListener
    * */
    override fun onConfigurationClick(config: Provision) {
        checkNFC(config)
    }

    override fun onConfigurationDelete(config: Provision) {

        val builder = AlertDialog.Builder(this)
        builder.setTitle("Are you sure?")
        builder.setMessage("Are you sure you want to delete this configuration?")

        builder.setPositiveButton(android.R.string.yes) { _, _ ->
            config.enrolmentToken?.let {
                configSharedPref.deleteConfiguration(it)
                val index = configs.indexOf(config)
                configs.removeAt(index)
                configAdapter.notifyItemRemoved(index)
            }

            toggleNoItemVisibility(configAdapter.itemCount == 0)
        }

        builder.setNegativeButton(android.R.string.no) { _, _ ->
        }

        builder.show()
    }

    override fun onConfigurationEdit(config: Provision) {
        ConfigurationDetailsActivity.start(this, config)
    }
}
