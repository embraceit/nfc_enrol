package com.embrace.nfcprovision.ui.configuration.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import com.daimajia.swipe.SimpleSwipeListener
import com.daimajia.swipe.SwipeLayout
import com.daimajia.swipe.adapters.RecyclerSwipeAdapter
import com.embrace.nfcprovision.Provision
import com.embrace.nfcprovision.R
import com.embrace.nfcprovision.utils.CSwipeLayout

class ConfigAdapter(
    private val configurations: ArrayList<Provision> = arrayListOf(),
    private val actionListener: ConfigActionListener
) : RecyclerSwipeAdapter<ConfigViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ConfigViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.list_item_configuration, parent, false)
        return ConfigViewHolder(view)
    }

    override fun getSwipeLayoutResourceId(position: Int): Int {
        return R.id.csl_container
    }

    override fun getItemCount() = configurations.count()

    override fun onBindViewHolder(holder: ConfigViewHolder, position: Int) {
        val config = configurations[position]
        holder.bind(config, actionListener)

        // Add swipe support
        (holder.itemView as? CSwipeLayout)?.apply {
            addSwipeListener(object : SimpleSwipeListener() {
                override fun onStartOpen(layout: SwipeLayout?) {
                    closeAllExcept(layout)
                }
            })
        }
    }
}

interface ConfigActionListener {
    fun onConfigurationClick(config: Provision)
    fun onConfigurationEdit(config: Provision)
    fun onConfigurationDelete(config: Provision)
}