package com.embrace.nfcprovision.ui.provision

import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.core.os.bundleOf
import com.embrace.nfcprovision.Provision
import com.embrace.nfcprovision.R
import com.embrace.nfcprovision.base.BaseActivity

class ConfigurationDetailsActivity : BaseActivity() {
    override val TAG: String = "ProvisionActivity"

    companion object {
        fun start(context: Context, provision: Provision) {
            val intent = Intent(context, ConfigurationDetailsActivity::class.java)
                .putExtra("data", bundleOf("provision" to provision))
            context.startActivity(intent)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_configuration_details)

        val provision = intent.getBundleExtra("data")?.getSerializable("provision")
        replaceWithFragment(
            R.id.container,
            ConfigurationDetailsFragment.newInstance(provision as Provision?)
        )
    }
}