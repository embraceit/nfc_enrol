package com.embrace.nfcprovision.ui.onboarding

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.widget.Button
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.viewpager.widget.ViewPager
import com.embrace.nfcprovision.R
import com.embrace.nfcprovision.ui.configuration.ConfigurationListActivity
import com.embrace.nfcprovision.utils.Animatoo
import com.embrace.nfcprovision.utils.AppPrefs
import com.embrace.nfcprovision.utils.Constants

class OnBoardingActivity : AppCompatActivity() {
    companion object {
        fun start(context: Context) {
            val intent = Intent(context, OnBoardingActivity::class.java)
            context.startActivity(intent)
        }
    }

    private lateinit var mViewPager: ViewPager
    private lateinit var textSkip: TextView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (AppPrefs(this).isFirstTimeLaunch()) {
            initialize()
        } else {
            ConfigurationListActivity.start(this, Constants.FLAG_NEW)
        }
    }

    private fun initialize() {

        setContentView(R.layout.activity_onboarding)

        mViewPager = findViewById(R.id.viewPager)
        mViewPager.adapter = OnBoardingViewPagerAdapter(supportFragmentManager, this)

        textSkip = findViewById(R.id.text_skip)
        textSkip.setOnClickListener {
            finish()
            ConfigurationListActivity.start(this)
            Animatoo.animateSlideLeft(this)
        }

        val btnNextStep: Button = findViewById(R.id.btn_next_step)

        btnNextStep.setOnClickListener {
            if (getItem(+1) > mViewPager.childCount - 1) {

                // mark on-boarding done on shared pref
                AppPrefs(this).setFirstTimeLaunch(false)

                // take user to configuration list screen
                ConfigurationListActivity.start(this)
                finish()

                Animatoo.animateSlideLeft(this)
            } else {
                mViewPager.setCurrentItem(getItem(+1), true)
            }
        }
    }

    private fun getItem(i: Int): Int {
        return mViewPager.currentItem + i
    }

}
