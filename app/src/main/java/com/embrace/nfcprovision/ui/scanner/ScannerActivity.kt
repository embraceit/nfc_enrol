package com.embrace.nfcprovision.ui.scanner

import android.Manifest
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import androidx.activity.result.contract.ActivityResultContracts
import androidx.core.content.ContextCompat
import com.embrace.nfcprovision.R
import com.embrace.nfcprovision.base.BaseActivity

class ScannerActivity : BaseActivity() {
    override val TAG: String = "ScannerActivity"

    companion object {
        fun start(context: Context) {
            val intent = Intent(context, ScannerActivity::class.java)
            context.startActivity(intent)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        getCameraPermission()
    }

    private val cameraPermission =
        registerForActivityResult(ActivityResultContracts.RequestPermission()) { permission ->
            if (permission) {
                setupView()
            } else {
                getCameraPermission()
            }
        }


    private fun getCameraPermission() {
        val checkPermission = ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA)
        if (checkPermission == PackageManager.PERMISSION_GRANTED) {
            setupView()
        } else {
            cameraPermission.launch(Manifest.permission.CAMERA)
        }
    }

    private fun setupView() {
        setContentView(R.layout.activity_configuration_details)
        replaceWithFragment(R.id.container, ScannerFragment.newInstance())
    }
}